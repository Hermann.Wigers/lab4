package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    
    private int cols;
    private int rows;
    private CellState[][] cells;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.cols = columns;
		this.rows = rows;
        this.cells = new CellState[rows][cols];
        
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                this.cells[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        
        return this.rows;
    }

    @Override
    public int numColumns() {
        
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        
        {
        if (row < 0 || row > this.rows || column < 0 || column > cols) {
            throw new IndexOutOfBoundsException();
        }
        
    }
    cells[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        
        if (row < 0 || row > this.rows || column < 0 || column > cols) {
            throw new IndexOutOfBoundsException();
        }
        return cells[row][column];
        
    }

    @Override
    public IGrid copy() {
        CellGrid copiedGrid = new CellGrid(rows, cols, CellState.DEAD);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                copiedGrid.set(i, j, get(i, j));
            }
        }
        return copiedGrid;
    }
    
}
